# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# make legacy vim use init.vim instead of ~/.vimrc
# export VIMINIT='source ~/.dotfiles/vim/init.vim'

# Per modern vim, assuming I have my XDG settings set up correctly

export VIMCONFIG=${XDG_CONFIG_HOME:-$HOME/.config}/vim
export VIMDATA=${XDG_DATA_HOME:-$HOME/.local/share}/vim

# Switching back to vim again...
for editor in vim; do
  if inpath ${editor}; then
    export EDITOR=${editor}
    alias vi=$editor
    alias vim=$editor
    break
  fi
done ; unset editor
