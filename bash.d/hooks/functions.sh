# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

function ods_content() {
  local filename="${1:?${FUNCNAME[0]} needs a filename}"
  local basename=`/usr/bin/basename "$filename" .ods`
  local dirname=`/usr/bin/dirname "$filename"`
  unzip -p "$dirname/$basename.ods" content.xml | xmlformat > "$dirname/$basename.xml"
}
