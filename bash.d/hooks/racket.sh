# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# This will likely break when we go from Racket 9 to Racket 10. That's
# another day...
racket=$(ls -d /Applications/Racket* 2>/dev/null | sort | tail -1)

if [ "$racket" ]; then
  export PATH="$PATH:$racket/bin"
fi
