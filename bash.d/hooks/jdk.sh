# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

if [[ -d "/usr/local/opt/openjdk/bin" ]]; then
  export PATH="/usr/local/opt/openjdk/bin:$PATH"
fi
