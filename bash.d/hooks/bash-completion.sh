# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# bail if executable is not present
[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] || return 1
export BASH_COMPLETION_COMPAT_DIR="/usr/local/etc/bash_completion.d"
source "/usr/local/etc/profile.d/bash_completion.sh"
