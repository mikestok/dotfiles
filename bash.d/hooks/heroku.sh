# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# Setup Instructions for HEROKU CLI Autocomplete ---
#
# 1) Add the autocomplete env var to your bash profile and source it
# $ printf "$(heroku autocomplete:script bash)" >> ~/.bashrc; source ~/.bashrc

# heroku autocomplete setup
HEROKU_AC_BASH_SETUP_PATH=/Users/mike/Library/Caches/heroku/autocomplete/bash_setup && test -f $HEROKU_AC_BASH_SETUP_PATH && source $HEROKU_AC_BASH_SETUP_PATH;
