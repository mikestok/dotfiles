# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# bail if executable is not present
[[ -x /usr/local/bin/asdf ]] || return 1

[[ -r "/usr/local/opt/asdf/asdf.sh" ]] || return 1

source "/usr/local/opt/asdf/asdf.sh" > /dev/null

# For compiling erlang using asdf so I don't have to have
# java/openjdk installed
export KERL_CONFIGURE_OPTIONS="--without-javac --with-ssl=$(brew --prefix openssl@1.1)"
# Make OTP include docs
export KERL_BUILD_DOCS=yes
