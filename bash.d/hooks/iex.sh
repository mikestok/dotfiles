# vim: set ft=sh:ts=2:sw=2:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

[[ -n ${1} ]] || return 1

if inpath iex; then
  function siex() {
    iex --sname ${IEX_SNAME:-iex$(date +%Y%m%d%H%M%S)_$$@localhost} --cookie ${IEX_COOKIE:-cookie} "$@"
}
fi
