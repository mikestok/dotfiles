# vim: set ft=sh:ts=2:sw=2:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

if [ -s "$HOME/.kiex/scripts/kiex" ]; then
  source "$HOME/.kiex/scripts/kiex"
fi
