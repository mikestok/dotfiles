# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# According to https://direnv.net/docs/hook.html this should be the last
# thing in .bashrc

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

if inpath direnv; then
  eval "$(direnv hook bash)"
fi
