# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# for "pip(3) install --user ..." executables
for d in "$HOME"/Library/Python/*/bin; do
  export PATH="$d:$PATH"
done
