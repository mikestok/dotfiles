# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# this script should only ever be sourced
[[ ${BASH_SOURCE[0]} != ${0} ]] || exit 1

# bail if executable is not present
[[ -x /usr/local/bin/fzf ]] || return 1

# auto-completion
source "/usr/local/opt/fzf/shell/completion.bash" 2> /dev/null

# key bindings
source "/usr/local/opt/fzf/shell/key-bindings.bash" 2> /dev/null

export FZF_DEFAULT_OPTS='--height 30% --border'

if inpath rg; then
  export FZF_DEFAULT_COMMAND='rg --files'
fi
