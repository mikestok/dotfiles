# vim: set ft=sh:ts=4:sw=4:noet:nowrap # bash

# enable color ls output
alias ls='command ls --color=auto'
