" vim:ft=vim:ts=2:sw=2:fdm=marker

" set runtimepath^=~/.vim runtimepath+=~/.vim/after
" let &packpath = &runtimepath
" source ~/.vim/vimrc


" ... back to the "real" vim with minimal plugins

" initialisation {{{

" set (local)leader to SPACE
let mapleader = " "
let maplocalleader = " "

" disable netrw (use dirvish instead)
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1

if has('vim_starting')
  let $MYVIMRC = resolve(expand('<sfile>'))
  let s:vimdir = fnamemodify($MYVIMRC, ':p:h')
  let s:tempdir = s:vimdir . '/tmp'
  let &directory = s:tempdir . '//'
  let &viewdir = s:tempdir . '/view'
  let &undodir = s:tempdir . '/undo'
  if !has('nvim')
    let &runtimepath = printf('%s,%s,%s/after', s:vimdir, &runtimepath, s:vimdir)
    if ! isdirectory(&g:undodir) | call mkdir(&g:undodir, 'p', 0700) | endif
    if ! isdirectory(&g:undodir) | call mkdir(&g:undodir, 'p', 0700) | endif
  endif
  let s:viminfo = has('nvim') ? 'main.shada' : 'viminfo'
  let &viminfo = "!,<800,'10,/50,:100,h,f0,n" . s:tempdir . '/' . s:viminfo
  "               | |    |   |   |    | |  + path to viminfo file
  "               | |    |   |   |    | + file marks 0-9,A-Z 0=NOT stored
  "               | |    |   |   |    + disable 'hlsearch' loading viminfo
  "               | |    |   |   + command-line history saved
  "               | |    |   + search history saved
  "               | |    + files marks saved
  "               | + lines saved each register (old name for <, vi6.2)
  "               + don't preserve the buffer list
endif
" }}}

" settings {{{
set   autoindent
set   autoread
set   cmdheight=2
set   colorcolumn=80
set   diffopt=vertical
set noerrorbells
set nofoldenable
set   foldlevelstart=99
set   foldmethod=syntax
set   guicursor=
set   history=500
set   hlsearch
set   ignorecase
set   incsearch
set nojoinspaces
set   laststatus=2
set   lazyredraw
set   list
set   listchars=tab:▸▹,extends:❯,precedes:❮,nbsp:⌴,trail:•
set   matchpairs+=<:>
set   mouse=a
set   number
set   numberwidth=5
set   showbreak=➧
set   showcmd
set   showmatch
set noshowmode
set   signcolumn=yes
set   smartcase
set   splitbelow
set   splitright
set nostartofline
set   tags^=./.git/tags
set   termguicolors
set   ttimeout
set   ttimeoutlen=-1
set   visualbell
set   virtualedit=block
set   whichwrap=b,s,h,l,<,>,[,]
set   wildignore+=*.o,*.obj,*.a
set   wildignore+=*/.git/*,*/.hg/*
set   wildignore+=*~,*.bak
set   wildchar=<Tab>
set   wildmenu
set   wildmode=longest,full
set   winminheight=0
set   wrap
" }}}

" tabs/spaces {{{
set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=2
" }}}

" plugins {{{
if has('vim_starting')
  packadd minpac
  call minpac#init()
  call minpac#add('morhetz/gruvbox')
  " call minpac#add('/usr/local/opt/fzf')
  call minpac#add('junegunn/fzf.vim')
  call minpac#add('w0rp/ale')
  if has('nvim')
    call minpac#add('Shougo/deoplete.nvim', {'do': ':UpdateRemotePlugins'})
  else
    call minpac#add('Shougo/deoplete.nvim')
    call minpac#add('roxma/nvim-yarp')
    call minpac#add('roxma/vim-hug-neovim-rpc')
  endif
  call minpac#add('itchyny/lightline.vim')
  call minpac#add('slashmili/alchemist.vim')
  call minpac#add('vim-scripts/tComment')
  call minpac#add('justinmk/vim-dirvish')
  call minpac#add('sheerun/vim-polyglot')
  call minpac#add('adelarsq/vim-matchit')
  call minpac#add('airblade/vim-gitgutter')
  call minpac#add('tpope/vim-surround')
  call minpac#add('tpope/vim-repeat')
  call minpac#add('tpope/vim-fugitive')
  call minpac#add('tpope/vim-endwise')
  call minpac#add('wellle/context.vim')
endif
" }}}

" colorscheme {{{
set background=light
set t_Co=256

if has('vim_starting')
  try
    " let g:gruvbox_contrast_dark = "hard"
    let g:gruvbox_contrast_light = "hard"
    let g:gruvbox_italic = 1
    let g:gruvbox_number_column = "bg1"
    colorscheme gruvbox
  catch
    colorscheme default
  endtry
endif
" highlight terminal cursor
if has('nvim')
  highlight! link TermCursor Cursor
  highlight! TermCursorNC guibg=red guifg=white ctermbg=1 ctermfg=15
endif
" }}}

" ale {{{
let g:ale_sign_error = "✗"
let g:ale_sign_warning = "?"
highlight link ALEWarningSign String
highlight link ALEErrorSign Title
" }}}

" Copy & paste {{{
"
" From https://www.reddit.com/r/neovim/comments/3fricd/easiest_way_to_copy_from_neovim_to_system/
" Copy to clipboard
" <Leader>c for copy (like command C)
" <Leader>v for paste (like command V)
vnoremap  <leader>c  "+y
nnoremap  <leader>c  "+yg_
nnoremap  <leader>c  "+y
nnoremap  <leader>c  "+yy

" Paste from clipboard
nnoremap <leader>v "+p
nnoremap <leader>v "+P
vnoremap <leader>v "+p
vnoremap <leader>v "+P
" }}}

" fzf {{{
set rtp+=/usr/local/opt/fzf
nmap ; :Buffers<CR>
nmap <Leader>r :Tags<CR>
nmap <Leader>t :Files<CR>
nmap <Leader>a :Ag<CR>"
" }}}

" deoplete {{{
" ... it's so useful I'll overlook it's consideration that "Python" is an
" improvement over VimL!
let g:deoplete#enable_at_startup = 1
" let g:deoplete#enable_smart_case = 1
let g:deoplete#disable_auto_complete = 1
set completeopt+=noinsert
" deoplete tab-complete
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
	inoremap <silent><expr> <TAB>
		\ pumvisible() ? "\<C-n>" :
		\ <SID>check_back_space() ? "\<TAB>" :
		\ deoplete#manual_complete()
		function! s:check_back_space() abort "{{{
		let col = col('.') - 1
		return !col || getline('.')[col - 1]  =~ '\s'
		endfunction"}}}
" }}}


" dirvish {{{
let g:dirvish_mode = ':sort r /[^\/]$/'
" }}}

" GitGutter styling to use · instead of +/- {{{
let g:gitgutter_sign_added = '∙'
let g:gitgutter_sign_modified = '∙'
let g:gitgutter_sign_removed = '∙'
let g:gitgutter_sign_modified_removed = '∙'
" }}}

" Lightline {{{
let g:lightline = {
\ 'active': {
\   'left': [['mode', 'paste'], ['filename', 'modified']],
\   'right': [['lineinfo'], ['percent'], ['readonly', 'linter_warnings', 'linter_errors', 'linter_ok']]
\ },
\ 'component_expand': {
\   'linter_warnings': 'LightlineLinterWarnings',
\   'linter_errors': 'LightlineLinterErrors',
\   'linter_ok': 'LightlineLinterOK'
\ },
\ 'component_type': {
\   'readonly': 'error',
\   'linter_warnings': 'warning',
\   'linter_errors': 'error'
\ },
\ }
function! LightlineLinterWarnings() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%d ?', all_non_errors)
endfunction
function! LightlineLinterErrors() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%d ✗', all_errors)
endfunction
function! LightlineLinterOK() abort
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '✓ ' : ''
endfunction

" Update and show lightline but only if it's visible (e.g., not in Goyo)
autocmd User ALELint call s:MaybeUpdateLightline()
function! s:MaybeUpdateLightline()
  if exists('#lightline')
    call lightline#update()
  end
endfunction
" }}}

" autocommands {{{
augroup MyAutoCommands
  autocmd!
  " Reload vimrc
  autocmd bufwritepost $MYVIMRC nested source $MYVIMRC
augroup END
augroup filetypedetect
  autocmd BufNew,BufNewFile,BufRead *.exs :setfiletype elixir
augroup END

augroup MyTerminalCommands
  autocmd TermOpen,BufEnter term://* :set nonumber
  autocmd TermOpen,BufEnter term://* :set scl=no
  autocmd TermOpen,BufEnter term://* :GitGutterDisable
  autocmd TermClose term://* set number
  autocmd TermClose term://* set scl=yes
  " autocmd TermClose term://* set :GitGutterEnable " not sure why this causes
  " "spurious" messages in things like FZF windows...
augroup END
" }}}

" other mappings {{{
" edit vimrc
map <leader>e :e! $MYVIMRC<cr>

" clear highlighted search
noremap <silent> <cr> :nohlsearch<cr><cr>

" Move around the location list (e.g. ale messages)
nnoremap <Leader>n :lnext<CR>
nnoremap <Leader>p :lprevious<CR>
nnoremap <Leader>r :lrewind<CR>

" Turn off linewise keys. Normally, the `j' and `k' keys move the cursor down one entire line. with
" line wrapping on, this can cause the cursor to actually skip a few lines on the screen because
" it's moving from line N to line N+1 in the file. I want this to act more visually -- I want `down'
" to mean the next line on the screen
nmap j gj
nmap k gk

" You don't know what you're missing if you don't use this.
nmap <C-e> :e#<CR>

" Move between open buffers.
nmap <C-n> :bnext<CR>
nmap <C-p> :bprev<CR>

" Emacs-like bindings in normal mode
nmap <C-x>0 <C-w>c
nmap <C-x>1 <C-w>o
nmap <C-x>1 <C-w>s
nmap <C-x>1 <C-w>v
nmap <C-x>o <C-w><C-w>
nmap <M-o>  <C-w><C-w>

" Emacs-like bindings in insert mode
imap <C-e> <C-o>$
imap <C-a> <C-o>0

" Emacs-like bindings in the command line from `:h emacs-keys`
cnoremap <C-a>  <Home>
cnoremap <C-b>  <Left>
cnoremap <C-f>  <Right>
cnoremap <C-d>  <Del>
cnoremap <C-e>  <End>
cnoremap <M-b>  <S-Left>
cnoremap <M-f>  <S-Right>
cnoremap <M-d>  <S-right><Delete>
cnoremap <Esc>b <S-Left>
cnoremap <Esc>f <S-Right>
cnoremap <Esc>d <S-right><Delete>
cnoremap <C-g>  <C-c>

" Use the space key to toggle folds
" nnoremap <space> za
" vnoremap <space> zf

" Super fast window movement shortcuts
nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-h> <C-W>h
nmap <C-l> <C-W>l

" Get out of terminal mode easily in nvim (Modern Vim)
if has('nvim')
  tnoremap <Esc> <C-\><C-n>
  tnoremap <C-v><Esc> <Esc>
endif
" }}}
