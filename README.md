dotfiles
========

Originally copied from https://github.com/jnalley/dotfiles

Install
-------

Clone:

    git clone git://github.com/mikestok/dotfiles.git ~/.dotfiles

Create symlinks and install vim plugins:

    ~/.dotfiles/install.sh
